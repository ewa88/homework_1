﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dziennik
{
    class Absence 
    {
        public enum TypeAbsence
        {
            o,
            n
        }
        public DateTime Date;
        public TypeAbsence Presence;
    }
}
