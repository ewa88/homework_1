﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace Dziennik
{
    class Program
    {
        private static Dictionary<long, Student> _register = new Dictionary<long, Student>();
        private static Dictionary<int, Course> _courseRegister = new Dictionary<int, Course>();
       


        static void Main(string[] args)

        {
            /*
               Course actualCourse = new Course();
            actualCourse.Id = 1;
            actualCourse.Name = "Junior C# Developer";
            actualCourse.TrainerName = "Jakub Bulczak";
            actualCourse.Date = DateTime.Parse("2017/04/25");
            actualCourse.HomeworkThreshold = Int32.Parse("80");
            actualCourse.PresenceThreshold = Int32.Parse("70");
            actualCourse.NumberOfStudents = Int32.Parse("3");
            _courseRegister[actualCourse.Id] = actualCourse;

            CourseStudents kursant1 = new CourseStudents();
            actualCourse._courseStudentDictionary.Add(1, kursant1);

            CourseStudents kursant2 = new CourseStudents();
            actualCourse._courseStudentDictionary.Add(2, kursant2);

            CourseStudents kursant3 = new CourseStudents();
            actualCourse._courseStudentDictionary.Add(3, kursant3);

            Student test = new Student();
                test.PESEL = 1;
                test.BirthDate = DateTime.Parse("1980/10/29");
                test.Name = "Magda";
                test.Surname = "Nowak";
                test.Sex = (Student.TypeSex)Enum.Parse(typeof(Student.TypeSex), ("K"));
                _register[test.PESEL] = test;


                Student test1 = new Student();
                test1.PESEL = 2;
                test1.BirthDate = DateTime.Parse("1987/12/15");
                test1.Name = "Tomek";
                test1.Surname = "Kowalski";
                test1.Sex = (Student.TypeSex)Enum.Parse(typeof(Student.TypeSex), ("M"));
                _register[test1.PESEL] = test1;

            Student test2 = new Student();
            test2.PESEL = 3;
            test2.BirthDate = DateTime.Parse("1970/01/24");
            test2.Name = "Kasia";
            test2.Surname = "Wisniewski";
            test2.Sex = (Student.TypeSex)Enum.Parse(typeof(Student.TypeSex), ("M"));
            _register[test2.PESEL] = test2;
            
                      // dodanie nowego obiektu homework

                     Homework homework = new Homework();
                         homework.MaxResult = 100;
                         actualCourse.HomeworkList.Add(homework);
                         homework._homeworkResult.Add(1, 80);
                         homework._homeworkResult.Add(2, 90);

                Homework homework1 = new Homework();
                         homework1.MaxResult = 100;
                         actualCourse.HomeworkList.Add(homework1);
                         homework1._homeworkResult.Add(1, 100);
                         homework1._homeworkResult.Add(2, 10); 

                //dodanie obiektu absence

               Absence absence = new Absence();
                absence.Date = DateTime.Parse("2017/05/04");
                absence.Presence = (Absence.TypeAbsence)Enum.Parse(typeof(Absence.TypeAbsence), ("o"));
                kursant1.AbsenceList.Add(absence);

                Absence absence1 = new Absence();
                absence1.Date = DateTime.Parse("2017/05/04");
                absence1.Presence = (Absence.TypeAbsence)Enum.Parse(typeof(Absence.TypeAbsence), ("n"));
                kursant2.AbsenceList.Add(absence1);

            Absence absence2 = new Absence();
            absence2.Date = DateTime.Parse("2017/05/04");
            absence2.Presence = (Absence.TypeAbsence)Enum.Parse(typeof(Absence.TypeAbsence), ("n"));
            kursant3.AbsenceList.Add(absence2);

    */

            string choose;
            bool x = true;


            while (x)
            {
                Console.WriteLine("Wybierz polecenie dla dziennika (addStudents/addCourse/" +
                                  "addPresence/addHomework/courseReport/exit):");
                choose = Console.ReadLine();

                switch (choose)
                {
                    case "addStudents":
                        AddStudent();
                        break;

                    case "addCourse":
                        AddCourse();
                        break;

                    case "addPresence":
                        AddPresence();
                        break;

                    case "addHomework":
                        StudentHomework();
                        break;

                    case "courseReport":
                        Report();
                        break;

                    case "Exit":
                        x = false;
                        break;

                    case "exit":
                        x = false;
                        break;

                    default:
                        Console.WriteLine("Bledna instrukcja ! ");
                        break;
                }
            } //main
        } //class

        private static void AddCourse()
        {
            bool properCourseDate;
            string answerDate;
            bool loop = true;
            string answerExit;
            string answerPesel;
            
            try
            {
                while (loop)
                {
                    Course course = new Course();
                    Console.WriteLine(
                            "Podaj nr porzadkowy kursu (jesli chcesz zaprzestac dodawania kursow wpisz exit): ");
                    answerExit = Console.ReadLine();
                    if (answerExit == "exit" || answerExit == "Exit")
                    {
                        loop = false;
                    }
                    else
                    {
                        course.Id = Int32.Parse(answerExit);
                        if (_courseRegister.ContainsKey(course.Id))
                        {
                            Console.WriteLine("Kurs o podanym Id juz istnieje. ");
                        }
                        else
                        {
                            Console.WriteLine("Podaj nazwe kursu: ");
                            course.Name = Console.ReadLine();
                            Console.WriteLine("Podaj nazwe imie i nazwisko trenera: ");
                            course.TrainerName = Console.ReadLine();
                            properCourseDate = false;
                            while (properCourseDate == false)
                            {
                                Console.WriteLine("Podaj date rozpoczecia kursu: ");
                                answerDate = Console.ReadLine();
                                try
                                {
                                    course.Date = DateTime.Parse(answerDate);
                                    properCourseDate = true;
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("Blednie wprowadzono date.");
                                    properCourseDate = false;
                                    }
                                }

                            Console.WriteLine("Podaj prog zaliczenia w procentach dla prac domowych:");
                            course.HomeworkThreshold = Int32.Parse(Console.ReadLine());
                            Console.WriteLine("Podaj prog dla obecnosci kursanta (w procentach): ");
                            course.PresenceThreshold = Int32.Parse(Console.ReadLine());
                            Console.WriteLine("Podaj liczbe kursantow w grupie: ");
                            course.NumberOfStudents = Int32.Parse(Console.ReadLine());
                            _courseRegister.Add(course.Id, course);

                            bool properPesel = false;
                            while (properPesel == false)
                            {
                                Console.WriteLine(
                                        "Wprowadz PESEL uczestnika kursu (jesli chcesz zakonczyc wpisz exit) : ");
                                answerPesel = Console.ReadLine();
                                    if (answerPesel == "exit" || answerPesel == "Exit")
                                    {
                                        properPesel = true;
                                    }
                                    else
                                    {
                                        if (long.Parse(answerPesel) <= 0 || long.Parse(answerPesel) >= 10)
                                        {
                                            Console.WriteLine("Blad. Wpisano niepoprawny PESEL. ");
                                        }
                                        else
                                        {
                                            if (course._courseStudentDictionary
                                                    .ContainsKey(long.Parse(answerPesel)))
                                            {
                                                    Console.WriteLine(
                                                        "Podany PESEL zostal juz przypisany do tego kursu. ");
                                            }
                                            else
                                            {
                                                if (_register.ContainsKey(long.Parse(answerPesel)))
                                                {
                                                    long pesel;
                                                    CourseStudents courseStudent = new CourseStudents();
                                                    pesel = long.Parse(answerPesel);
                                                    course._courseStudentDictionary.Add(pesel, courseStudent);
                                                    //Console.WriteLine("dictionary count = " + course._courseStudentDictionary.Count + ", number of students = " + course.NumberOfStudents);
                                                    if (course.NumberOfStudents == course._courseStudentDictionary.Count)
                                                    {
                                                        Console.WriteLine("Podano juz zadeklarowana liczbę kursantow. ");
                                                        properPesel = true;
                                                    }
                                            }
                                                else
                                                {
                                                    Console.WriteLine(
                                                            "Wprowadzono PESEL kursanta, ktorego nie ma w bazie. ");
                                                }
                                            }
                                        }
                                    }
                            }
                        }
                    }
                }
               
            }

        catch (Exception e)
            {
                Console.WriteLine("Blad podawanych danych");
            }
        }

        private static void AddStudent()
        {
            bool jumpFromAddStudent = true;
            string answer;
            long pesel;
            bool properDate;
            string answerDate;
          
            try
            {                  
                while (jumpFromAddStudent)
                {

                    Console.WriteLine("Podaj PESEL kursanta (jesli chcesz przerwac wpisz exit): ");
                    answer = Console.ReadLine();
                    if (answer == "exit" || answer == "Exit")
                    {
                        jumpFromAddStudent = false;
                    }
                    else
                    {                                          
                        pesel = Int32.Parse(answer);
                        if (pesel <= 0 || pesel >= 10)
                        {
                            Console.WriteLine("Blad. Wpisano niepoprawny PESEL. ");
                        }
                        else
                        {
                            if (_register.ContainsKey(pesel))
                            {
                                Console.WriteLine("Blad. Student o podanym PESELu zostal juz wprowadzony. ");
                            }

                            else
                            {
                                Student student = new Student();
                                student.PESEL = pesel;
                                Console.WriteLine("Podaj imie kursanta: ");
                                student.Name = Console.ReadLine();
                                Console.WriteLine("Podaj nazwisko kursanta: ");
                                student.Surname = Console.ReadLine();
                                properDate = false; //przypisuje boolowi fałsz
                                while (properDate==false) // tak dlugo jak bool jest nieprawda
                                {
                                    
                                    Console.WriteLine("Podaj date urodzenia kursanta: "); //wczytuje date
                                    answerDate = Console.ReadLine();
                                    try
                                    {
                                        student.BirthDate = DateTime.Parse(answerDate); // probuje przypisac do formatu DateTime
                                        properDate = true; // 
                                    }
                                    catch (Exception e)
                                    {
                                        properDate = false;
                                        Console.WriteLine("Blednie wprowadzono date. ");

                                    }
                                   
                                }
                                Console.WriteLine("Podaj płeć kursanta(K/M): ");
                                string sex = Console.ReadLine();
                                student.Sex = (Student.TypeSex) Enum.Parse(typeof(Student.TypeSex), (sex.ToUpper()));
                                _register.Add(student.PESEL, student);
                            }
                        }
                    }
                }
            }
                
            

            catch (Exception e)
            {
                Console.WriteLine("Blad podawanych danych");
            }

        }
        private static void AddPresence()
        {
            
            if (_courseRegister.Count > 0)
            {
                try
                {
                    DateTime Date = DateTime.Parse( "1999/01/01"); //musi cus byc bo resharper nie ogarnia :(
                    int courseId;

                    bool properAbsenceDate = true;
                    while (properAbsenceDate)
                    {
                        Console.WriteLine("Podaj date: ");
                        try
                        {
                            Date = DateTime.Parse(Console.ReadLine());
                            properAbsenceDate = false;
                        }
                        catch (Exception e)
                        {
                            properAbsenceDate = true;
                            Console.WriteLine("Podano niepoprawna date. ");
                        }
                    }

                    Console.WriteLine("Podaj Id kursu, dla ktorego chcesz przypisac obecnosc kursantow");
                        courseId = Int32.Parse(Console.ReadLine());             
                    
                        foreach (var courseStudent in _courseRegister[courseId]._courseStudentDictionary)
                        {

                            Absence absence = new Absence();
                            absence.Date = Date;                         
                            Console.WriteLine("Wpisz obecnosc (o-obecny, n-nieobecny) dla kursatna o numerze: " +
                                              courseStudent.Key);
                            absence.Presence =
                                (Absence.TypeAbsence) Enum.Parse(typeof(Absence.TypeAbsence), (Console.ReadLine()));
                              courseStudent.Value.AbsenceList.Add(absence);

                        }
                    }
                
                catch (Exception e)
                {
                    Console.WriteLine("Blad podawanych danych");
                }
            }
            else
            {
                Console.WriteLine("Blad. Nie dodano studentow lub kursu.");
            }
        }

        private static void StudentHomework()
        {
            if (_courseRegister.Count > 0)
            {
                try
                {
                    int courseId;
                    Console.WriteLine("Podaj Id kursu, dla ktorego chcesz przypisac wyniki zadan domowych");
                    courseId = Int32.Parse(Console.ReadLine());

                    if (_courseRegister.ContainsKey(courseId))
                    {
                        Homework homework = new Homework();
                        Console.WriteLine("Podaj maksymalna liczbe punktow do uzyskania z pracy domowej: ");
                        homework.MaxResult = Int32.Parse(Console.ReadLine());
                        _courseRegister[courseId].HomeworkList.Add(homework);

                        int wynik = 0;
                        foreach (var courseStudent in _courseRegister[courseId]._courseStudentDictionary)
                        {

                            Console.WriteLine("Podaj ilosc punktow uzyskanych przez studenta "+ _register[courseStudent.Key].Name+" " + _register[courseStudent.Key].Surname + " o numerze PESEL: " +
                                             courseStudent.Key);
                            wynik = Int32.Parse(Console.ReadLine());
                            homework._homeworkResult.Add(courseStudent.Key, wynik);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Podano Id nieistniejacego kursu");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Blad podawanych danych");
                }
            }
            else
            {
                Console.WriteLine("Blad. Nie dodano studentow lub kursu.");
            }
        }

        private static void Report()
        {
            foreach (var course in _courseRegister)
            {
                Console.WriteLine("Nazwa kursu: " + course.Value.Name);
                Console.WriteLine("Data rozpoczecia kursu: " + course.Value.Date);
                Console.WriteLine("Trener: " + course.Value.TrainerName);
                Console.WriteLine("Prog dla obecnosci: " + course.Value.PresenceThreshold);
                Console.WriteLine("Prog dla prac domowych: " + course.Value.HomeworkThreshold);


                //wypisywanie obecnosci

                Console.WriteLine("\nPresence: ");

                
                foreach (var courseStudent in course.Value._courseStudentDictionary)
                {
                    int n = courseStudent.Value.GetPresenceDaysNumber();
                    int y = courseStudent.Value.GetDaysNumber();
                    int wynik;
                    string z;

                    if (y == 0)
                    {
                        wynik = 0;
                        z = "Niezaliczone";
                    }
                    else
                    {
                        wynik = n * 100 / y;


                        if (wynik >= course.Value.HomeworkThreshold)
                        {
                            z = "Zaliczone";
                        }
                        else
                        {
                            z = "Niezaliczone";
                        }
                    }

                    Console.Write(_register[courseStudent.Key].Name + " ");
                    Console.Write(_register[courseStudent.Key].Surname + " ");
                    Console.WriteLine(n + "/" + y + " (" + wynik + "%) - " + z);
                }

                Console.WriteLine("\nHomework: ");
                
                    foreach (var courseStudent in course.Value._courseStudentDictionary)
                    {
                        Console.Write(_register[courseStudent.Key].Name + " " + _register[courseStudent.Key].Surname + " ");

                        int sumaResult = 0, x;
                        int sumaMaxResult = 0, procent;
                        int y = 0;

                        foreach (var homework in course.Value.HomeworkList)
                        {

                            if (homework._homeworkResult.ContainsKey(courseStudent.Key))
                            {
                                x = homework._homeworkResult[courseStudent.Key];
                            }
                            else
                            {
                                x = 0;
                            }

                            sumaResult = sumaResult + x;
                            y = homework.MaxResult;
                            sumaMaxResult = sumaMaxResult + y;

                        }
                        string r;


                        if (sumaMaxResult == 0)
                        {
                            procent = 0;
                            r = "Niezaliczone";
                        }
                        else
                        {
                            procent = sumaResult * 100 / sumaMaxResult;

                            if (procent >= course.Value.HomeworkThreshold)
                            {
                                r = "Zaliczone";
                            }

                            else
                            {
                                r = "Niezaliczone";
                            }
                        }

                        Console.WriteLine(sumaResult + "/" + sumaMaxResult + " (" + procent + "%) " + " - " + r);
                    }

                
            }//koniec course
            Console.WriteLine("");
            

        }
    }
}

        
    

        

        
            
            
    
    


