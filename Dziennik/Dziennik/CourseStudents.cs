﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dziennik
{
    class CourseStudents
    {
        public long Pesel;

        public List<Absence> AbsenceList = new List<Absence>();
        

       public int GetPresenceDaysNumber()
        {
            int i = 0;
            foreach (var log in AbsenceList)
            {

                if (log.Presence == (Absence.TypeAbsence)Enum.Parse(typeof(Absence.TypeAbsence), ("o")))
                {
                    i++;
                }
            }
            return i;
        }

        public int GetDaysNumber()
        {

            return AbsenceList.Count;
        }
    }
}
