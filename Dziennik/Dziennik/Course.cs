﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dziennik
{
    class Course
    {
        public int Id;
        public string Name;
        public string TrainerName;
        public DateTime Date;
        public int HomeworkThreshold;
        public int PresenceThreshold;
        public int NumberOfStudents;
        public long StudentPesel;

        public Dictionary<long, CourseStudents> _courseStudentDictionary = new Dictionary<long, CourseStudents>();

        public List<Homework> HomeworkList = new List<Homework>();
    }
}
